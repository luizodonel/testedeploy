import { Component, Input, Output } from '@angular/core';
import { DeputadoServiceService } from '../service/deputado-service.service';

@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.component.html',
  styleUrls: ['./gastos.component.css']
})
export class GastosComponent {

  listaDespesas = new Array();
  deputadoobjeto: any;
  redeSociais = new Array();
  pag = 1 ;
  contador = 5;

  constructor(private service: DeputadoServiceService) {

  }

  ngOnInit() {
    this.consultarDespesas(this.service.idDeputado);
    this.consultarDeputadoId(this.service.idDeputado);

  }

  consultarDeputadoId(id: number) {
    this.service.consultarDeputadoId(id).subscribe(
      (response) => {

          this.deputadoobjeto = response.dados;
          this.redeSociais = response.dados.redeSocial;
        
      },
      err => {
        console.log("Errouuuuuu")
      }
    );
  }

  consultarDespesas(id: number) {
    this.service.getdespesas(id).subscribe(
      (response) => {

        for (let index = 0; index < response.dados.length; index++) {
          this.listaDespesas.push(response.dados[index]);
        }
        console.log(this.listaDespesas)
      },
      err => {
        console.log("Errouuuuuu")
      }
    );
    
  }

}
