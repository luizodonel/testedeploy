import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DeputadoComponent } from './deputado/deputado.component';
import { DeputadoServiceService } from './service/deputado-service.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { GastosComponent } from './gastos/gastos.component';
import { RodapeComponent } from './rodape/rodape.component';
import { MenuComponent } from './menu/menu.component';
import { TipoDespesasComponent } from './tipo-despesas/tipo-despesas.component';
const routes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    DeputadoComponent,
    GastosComponent,
    RodapeComponent,
    MenuComponent,
    TipoDespesasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule.forRoot(routes)
    
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent,DeputadoServiceService]
})
export class AppModule { }
