import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DeputadoServiceService } from '../service/deputado-service.service';
import { TipoDespesaService } from '../service/tipo-despesa.service';

@Component({
  selector: 'app-tipo-despesas',
  templateUrl: './tipo-despesas.component.html',
  styleUrls: ['./tipo-despesas.component.css']
})
export class TipoDespesasComponent {

  
  listaDespesas = new Array();
  pag = 1;
  contador = 5;
  nomeDeputado: string = "";

  constructor(private service: TipoDespesaService, private router: Router) { }


  ngOnInit(): void {
    this.getlistaDespesas();
  }


  getlistaDespesas() {

    this.service.getlistaDespesas().subscribe(
      (response) => {

        for (let index = 0; index < response.dados.length; index++) {
          this.listaDespesas.push(response.dados[index]);
        }
      },
      err => {
        console.log("Errouuuuuu")
      }
    );

    console.log(this.listaDespesas);
  }

}
