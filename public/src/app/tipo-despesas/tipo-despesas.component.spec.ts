import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoDespesasComponent } from './tipo-despesas.component';

describe('TipoDespesasComponent', () => {
  let component: TipoDespesasComponent;
  let fixture: ComponentFixture<TipoDespesasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TipoDespesasComponent]
    });
    fixture = TestBed.createComponent(TipoDespesasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
