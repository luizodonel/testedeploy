import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeputadoComponent } from './deputado/deputado.component';
import { GastosComponent } from './gastos/gastos.component';
import { TipoDespesasComponent } from './tipo-despesas/tipo-despesas.component';

const routes: Routes = [
  { path: '', component: DeputadoComponent },
  { path: 'gastos', component: GastosComponent },
  { path: 'tipoDespesas', component: TipoDespesasComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
