import { Component, Input, Output } from '@angular/core';
import { DeputadoServiceService } from '../service/deputado-service.service';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-deputado',
  templateUrl: './deputado.component.html',
  styleUrls: ['./deputado.component.css']
})
export class DeputadoComponent {

  listaDeputados = new Array();
  pag = 1;
  contador = 5;
  nomeDeputado: string = "";

  constructor(private service: DeputadoServiceService, private router: Router) { }


  ngOnInit(): void {
    this.getListaDeputados();
  }

  gastos(id: number) {
    this.service.idDeputado = id;
    console.log(this.service.idDeputado)

    this.router.navigateByUrl("/gastos");
  }

  pesquisaDeputadoNome(nome: string) {
   
    this.service.consultarDeputadoNome(nome).subscribe(
      (response) => {
        this.listaDeputados = [];
        for (let index = 0; index < response.dados.length; index++) {
          this.listaDeputados.push(response.dados[index]);
        }
      },
      err => {
        console.log("Errouuuuuu")
      }
    );
  }

  getListaDeputados() {

    this.service.getListaDeputados().subscribe(
      (response) => {

        for (let index = 0; index < response.dados.length; index++) {
          this.listaDeputados.push(response.dados[index]);
        }
      },
      err => {
        console.log("Errouuuuuu")
      }
    );

    console.log(this.listaDeputados);
  }

}
