import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.';

@Injectable({
  providedIn: 'root'
})
export class DeputadoServiceService {

  idDeputado: number = 0;

  baseURL: string = environment.API;

  constructor(private http: HttpClient) { }

  getListaDeputados(): Observable<any> {
    return this.http.get(this.baseURL + "camara/listarDeputados")
  }

  getdespesas(id: number): Observable<any> {
    return this.http.get(this.baseURL + "camara/pesquisaDeputadoDespesas/?id=" + id)
  }

  consultarDeputadoId(id: number): Observable<any> {
    return this.http.get(this.baseURL + "camara/pesquisaDeputadoId/?id=" + id)
  }

  consultarDeputadoNome(nome: string): Observable<any> {
    return this.http.get(this.baseURL + "camara/pesquisaDeputadoNome/?nome=" + nome)
  }

}
