import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.';

@Injectable({
  providedIn: 'root'
})
export class TipoDespesaService {

  baseURL: string = environment.API;

  constructor(private http: HttpClient) { }

  getlistaDespesas(): Observable<any> {
    return this.http.get(this.baseURL + "camara/tipoDespesa")
  }
  
}
